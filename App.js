import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import AppNavigator from '@navigation/index';
import { NativeBaseProvider } from "native-base";
import AppProvider from '@context/appContext';
import { theme } from '@theme/index';

export default App = () => (
  <NativeBaseProvider theme={theme}>
    <NavigationContainer>
      <AppProvider>
        <AppNavigator />
      </AppProvider>
    </NavigationContainer>
  </NativeBaseProvider>
);
