import WrapperMock from '@mocks/providersWrapper';
import renderer from 'react-test-renderer';
import Wallet from '@views/Wallet';

describe('Wallet snashoot component', () => {
  it('should render component', () => {
    const tree = renderer
      .create(<WrapperMock><Wallet/></WrapperMock>)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
})
