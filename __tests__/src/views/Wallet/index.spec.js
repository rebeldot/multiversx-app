import WrapperMock, { APP_CONTEXT_MOCK } from '@mocks/providersWrapper';
import { render, fireEvent, within } from '@testing-library/react-native';
import Wallet from '@views/Wallet';
import { CURRENCY } from '@env';
import { act } from 'react-test-renderer';

const translation = require("@config/translation.json");

const mockNavigate = jest.fn();

jest.mock('@multiversx/sdk-dapp/utils/operations', () => ({
  formatAmount: () => {},
}));

jest.mock('@react-navigation/native', () => ({
  ...jest.requireActual('@react-navigation/native'),
  useNavigation: () => ({
    navigate: mockNavigate
  })
}));

const WrapperMockWithData = ({ children}) => <WrapperMock values={APP_CONTEXT_MOCK} >{children}</WrapperMock>;

describe('Wallet component', () => {
  it('should render component', () => {
    const { getByTestId, findByText } = render(<Wallet />, { wrapper: WrapperMock });
    
    expect(getByTestId('walletWrapper')).toBeTruthy();
    expect(findByText(translation.general.myWallet)).toBeTruthy();
    expect(findByText(`${translation.general.address}:`)).toBeTruthy();
    expect(findByText(`${translation.general.balance}:`)).toBeTruthy();

    expect(getByTestId('sendTransactionButton')).toBeTruthy();
    const { findByText: findByTextInButton } = within(getByTestId('sendTransactionButton'));

    expect(findByTextInButton(`${translation.general.send} ${translation.general.transaction}`)).toBeTruthy();
  });

  describe('when not having transactions & walletData', () => {
    it('should render balance with 0', () => {
      const { getByTestId } = render(<Wallet />, { wrapper: WrapperMock });

      const { findByText } = within(getByTestId('balance'));
      expect(findByText('0 ' + CURRENCY)).toBeTruthy();
    });

    it('should not have address', () => {
      const { getByTestId } = render(<Wallet />, { wrapper: WrapperMock });

      expect(getByTestId('address').props.children).toBe('');
    });

    it('should show no transactions text', () => {
      const { findByText } = render(<Wallet />, { wrapper: WrapperMock });

      expect(findByText(translation.transaction.errors.noTransactions)).toBeTruthy();
    });
  })

  describe('when having transactions & walletData', () => {
    it('should render balance of 10000', () => {
      const { getByTestId } = render(<Wallet />, { wrapper: WrapperMockWithData });

      const { findByText } = within(getByTestId('balance'));
      expect(findByText(`${APP_CONTEXT_MOCK.myWalletData.balance} ${CURRENCY}`)).toBeTruthy();
    });

    it('should have address', () => {
      const { getByTestId } = render(<Wallet />, { wrapper: WrapperMockWithData });

      const { findByText } = within(getByTestId('address'));
      expect(findByText(APP_CONTEXT_MOCK.myWalletData.address)).toBeTruthy();
    });

    it('should show transaction container', () => {
      const { findByText, queryAllByTestId } = render(<Wallet />, { wrapper: WrapperMockWithData });

      expect(findByText(`Last ${APP_CONTEXT_MOCK.transactions.length} transactions for account:`)).toBeTruthy();
      expect(queryAllByTestId('transactionItem').length).toBe(1);
    });
  })

  it('should navigate to transaction screen on "send transaction" press', () => {
    const { getByTestId } = render(<Wallet />, { wrapper: WrapperMockWithData });

    act(() => {
      fireEvent.press(getByTestId('sendTransactionButton'));
    });

    expect(mockNavigate).toHaveBeenCalledWith('TransactionTransfer');
  })
});