import { formatAmount } from '@multiversx/sdk-dapp/utils/operations/formatAmount';

describe('mnemonicWords', () => {
  it('should have mnemonic words', () => {
    expect(formatAmount({ input: '1000000000000000000' })).toBe("1");
  });
});