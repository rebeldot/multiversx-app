import { renderHook, act, waitFor } from '@testing-library/react-native';
import useToken from "@hooks/useToken";
import WrapperMock from '@mocks/providersWrapper';
import { APP_CONTEXT_MOCK } from '@mocks/providersWrapper';

const MOCKED_TOKEN = 'token';
const mockGetNativeAuthConfig = jest.fn().mockReturnValue(({
  origin: '',
  apiAddress: ''
})).mockName('getNativeAuthConfig');
const mockInitialize = jest.fn().mockResolvedValue(MOCKED_TOKEN);
const mockGetToken = jest.fn().mockReturnValue(MOCKED_TOKEN);
const mockSerializeForSigning = jest.fn().mockReturnValue('serialized');

jest.mock('@multiversx/sdk-dapp/services/nativeAuth/nativeAuth', () => ({
  nativeAuth: () => ({
    getNativeAuthConfig: mockGetNativeAuthConfig,
    initialize: mockInitialize,
    getToken: mockGetToken
  })
}));

jest.mock('@multiversx/sdk-core/out', () => ({
  SignableMessage: jest.fn().mockImplementation(() => ({
    serializeForSigning: mockSerializeForSigning
  }))
}));

describe('useToken hook', () => {
  describe('not having address in context', () => {
    it('should not get any token', () => {
      const { result } = renderHook(() => useToken(), { wrapper: WrapperMock });
  
      expect(result.current).toBe('');
    });
  
    it('should not call getToken & getNativeAuthConfig', () => {
      renderHook(() => useToken(), { wrapper: WrapperMock });
  
      expect(mockGetNativeAuthConfig).not.toBeCalled();
      expect(mockInitialize).not.toBeCalled();
      expect(mockGetToken).not.toBeCalled();
    });
  });

  describe('having address in context', () => {
    it('should call getNativeAuthConfig', async () => {
      renderHook(() => useToken(), {
        wrapper: ({ children }) => <WrapperMock values={APP_CONTEXT_MOCK}>{children}</WrapperMock>
      });

      expect(mockGetNativeAuthConfig).toHaveBeenCalledWith(true);
      await waitFor(() => {
        expect(mockInitialize).toBeCalled();
      });

      expect(mockGetToken).toBeCalledWith({
        address: APP_CONTEXT_MOCK.myWalletData.address.value,
        token: MOCKED_TOKEN,
        signature: 'serialized'
      });
    });

    it('should get token', async () => {
      const { result } = renderHook(() => useToken(), {
        wrapper: ({ children }) => <WrapperMock values={APP_CONTEXT_MOCK}>{children}</WrapperMock>
      });

      await waitFor(() => {
        expect(mockInitialize).toBeCalled();
      });

      expect(result.current).toBe(MOCKED_TOKEN);
    });
  });
})