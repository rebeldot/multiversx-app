import { mnemonicValidation, mnemonicWordsSanitizer } from "@utils/wallet";

jest.mock('@multiversx/sdk-wallet/out', () => ({
  UserSigner: jest.fn(),
}));

jest.mock('react-native-fs', () => ({
  writeFile: jest.fn(),
  mkdir: jest.fn(),
  readFile: jest.fn(),
  DocumentDirectoryPath: '/'
}));

const MOCKED_2_WORDS = "1. Horse 2. Window";
const MOCKED_24_WORDS = `
  1 year
  2 universe
  3 rural
  4 crush
  5 state
  6 unusual
  7 silver
  8 one
  9 choose
  10 unfold
  11 quarter
  12 opera
  13 certain
  14 uncle
  15 industry
  16 moon
  17 hold
  18 toast
  19 erode
  20 atom
  21 trumpet
  22 match
  23 bike
  24 bulk
`;

const translation = require('@config/translation');

describe('mnemonicValidation method', () => {
  it('should return error if field is empty', () => {
    const { error, mnemonic } = mnemonicValidation('');

    expect(error).toBe(translation.general.errors.requiredInput);
    expect(mnemonic).toBe('');
  });

  it('should return error if given text is less than 24 words', () => {
    const { error, mnemonic } = mnemonicValidation(MOCKED_2_WORDS);

    expect(error).toBe(translation.general.errors.textLength);
    expect(mnemonic).toBe('');
  });

  it('should return error if 24 words were not indetified', () => {
    const MOCKED_23_WORDS = MOCKED_24_WORDS.replace('24 bulk', '');
    const { error, mnemonic } = mnemonicValidation(MOCKED_23_WORDS);

    expect(error).toBe(translation.wallet.errors.mnemonicWordsLength);
    expect(mnemonic).toBe(mnemonicWordsSanitizer(MOCKED_23_WORDS).words);
    expect(23).toBe(mnemonicWordsSanitizer(MOCKED_23_WORDS).wordsCount);
  });

  
  it('should return no error and 24 mnemonic words', () => {
    const { error, mnemonic } = mnemonicValidation(MOCKED_24_WORDS);

    expect(error).toBe('');
    expect(mnemonic).toBe(mnemonicWordsSanitizer(MOCKED_24_WORDS).words);
  });
});