module.exports = {
  presets: ['module:metro-react-native-babel-preset',],
  plugins: [
    [
      "module-resolver",
      {
        alias: {
          '@components': "./src/components",
          '@mocks': "./__mocks__",
          '@views': "./src/views",
          '@navigation': './src/navigation',
          '@hooks': './src/hooks',
          '@theme': './src/assets/theme',
          '@images': './src/assets/images',
          '@utils': './src/utils',
          '@config': './src/config',
          '@context': './src/context',
          'crypto': 'react-native-quick-crypto',
          'stream': 'stream-browserify',
          'fs': 'react-native-fs',
          'buffer': '@craftzdog/react-native-buffer',
        },
      },
    ],
    [
      "module:react-native-dotenv",
      {
        "envName": "APP_ENV",
        "moduleName": "@env",
        "path": ".env",
        "safe": false,
        "allowUndefined": true,
        "verbose": false
      }
    ],
  ],
};
