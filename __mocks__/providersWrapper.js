import { NativeBaseProvider } from "native-base";
import { NavigationContainer } from '@react-navigation/native';
import { AppContext } from '@context/appContext';

export const MOCKED_ADDRESS = 'erd19vfcgtpvqculcfg73sjsf3zfwuh528x62kj9jps2xh6qskymu4yqha05k3';
export const EMPTY_APP_CONTEXT_MOCK = {
  myWalletData: {
    address: {
      value: '',
    },
    balance: ''
  },
  transactions: []
}

export const APP_CONTEXT_MOCK = {
  myWalletData: {
    address: {
      value: MOCKED_ADDRESS,
    },
    balance: '1000000000'
  },
  transactions: [{
    txHash: 'ca496cbffeebc203d96379525927ef282f822b009529442545fb7c290e0ec731',
    value: '1000000000000000000',
    "timestamp": 1684228092,
    status: "success",
  }]
}

const inset = {
  frame: { x: 0, y: 0, width: 0, height: 0 },
  insets: { top: 0, left: 0, right: 0, bottom: 0 },
};

const WrapperMock = ({ children, values = EMPTY_APP_CONTEXT_MOCK }) => (
  <NativeBaseProvider initialWindowMetrics={inset}>
    <NavigationContainer>
      <AppContext.Provider value={values}>
        {children}
      </AppContext.Provider>
    </NavigationContainer>
  </NativeBaseProvider>

);

export default WrapperMock;
