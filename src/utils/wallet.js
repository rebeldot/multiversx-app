import { UserSigner } from '@multiversx/sdk-wallet/out';
import fs, { DocumentDirectoryPath } from 'react-native-fs';

const translation = require('@config/translation');

const filesPath = DocumentDirectoryPath + '/wallet';

export function mnemonicValidation(text) {
  let error = '';
  let mnemonic = '';

  if (!text) {
    error = translation.general.errors.requiredInput;
  } else if (text.length < 24) {
    error = translation.general.errors.textLength;
  }
  

  if (!error) {
    ({ words: mnemonic, wordsCount } = mnemonicWordsSanitizer(text));

    if (wordsCount < 24) {
      error = translation.wallet.errors.mnemonicWordsLength;
    }
  }

  return {
    error,
    mnemonic
  }
}

/*
  This should be used to sanitize a text to be considered as mnemonic words
*/
export function mnemonicWordsSanitizer(text) {
  const exp = /[a-zA-Z]+/mgi

  text = text.match(exp);

  return {
    wordsCount: text.length,
    words: text.join(' ')
  };
}

/*
  Search for existing wallet files
*/
export async function saveWalletFile(address, wallet) {
  if (!address || !wallet) {
    Promise.reject('Address or wallet not provided');
  }

  fs.writeFile(
    `${filesPath}/${address}.json`,
    JSON.stringify(wallet)
  )
    .then(success => console.log(success))
    .catch(error => console.error(error));

  Promise.resolve();
}

/*
  Search for existing wallet files
*/
export async function getExistingWalletFiles() {
  const dirExists = await fs.exists(filesPath);

  if (!dirExists) {
    fs.mkdir(filesPath);
    return [];
  }

  return await fs.readDir(filesPath);
}

export async function getWalletFromExistingFile(address, password) {
  let fileContent = await fs.readFile(`${filesPath}/${address}.json`, { encoding: "utf8" });

  fileContent = fileContent ? JSON.parse(fileContent) : null;

  const wallet = UserSigner.fromWallet(fileContent, password);

  return wallet;
}