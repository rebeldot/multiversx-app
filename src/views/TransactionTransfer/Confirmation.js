import Ract from 'react';
import { Box, Button, Center, Link, Text } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome5';
import PageTitle from '@components/structure/PageTitle';
import { ELEMENT_SPACING } from '@theme/sizes';
import Price from '@components/Price';
import { useNavigation } from '@react-navigation/native';
import { Linking } from 'react-native';
import { DEVNET_EXPLORER_URL } from '@env';

const translation = require('@config/translation');

const Confirmation = ({
  amount,
  receiver,
  txHash
}) => {
  const navigation = useNavigation();

  const onViewInExplorerPress = () => Linking.openURL(`${DEVNET_EXPLORER_URL}/transactions/${txHash}`);

  const onBackToWalletPress = () => {
    navigation.navigate('Wallet');
  };

  return (
    <Box flex={1} marginY={ELEMENT_SPACING.LG}>
      <Center flex={1} justifyContent="flex-start">
        <PageTitle title="Confirmation" />
        <Box margin={ELEMENT_SPACING.XL}>
          <Icon name="check-circle" color="green" size={90} />
        </Box>
        <Price price={amount} />
        <Center marginBottom={ELEMENT_SPACING.LG}>
          <Text color="grey">{translation.transaction.successfullySentTo}:</Text>
          <Text textAlign="center">{receiver}</Text>
        </Center>
        <Center marginBottom={ELEMENT_SPACING.LG}>
          <Text color="grey">TX {translation.general.hash}:</Text>
          <Text textAlign="center">{txHash}</Text>
        </Center>
        <Link onPress={onViewInExplorerPress}>{translation.general.viewInExplorer}</Link>
      </Center>
      <Button onPress={onBackToWalletPress}>{translation.wallet.backToWallet}</Button>
    </Box>
  )
}

export default Confirmation;
