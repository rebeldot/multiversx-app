import PageTitle from '@components/structure/PageTitle';
import React, { useState } from 'react';
import BaseInput from '@components/form/BaseInput';
import ContentWrapper from '@components/structure/ContentWrapper';
import { Box, Button, Text } from 'native-base';
import useMultiversXApi from '@hooks/useMultiversXApi';
import Confirmation from './Confirmation';
import { ELEMENT_SPACING } from '@theme/sizes';
import { useAppContext } from '@context/appContext';
import { Address } from '@multiversx/sdk-core';
import { formatAmount } from '@multiversx/sdk-dapp/utils/operations';
import useSignTransaction from '@hooks/useSignTransaction';
import BaseModal from '@components/structure/BaseModal';
import { CURRENCY } from '@env';

const translation = require('@config/translation');

const TransactionTransfer = () => {
  const [receiver, setReceiver] = useState('');
  const [amount, setAmount] = useState('');
  const [signPassword, setSignPassword] = useState('');
  const [transactionHash, setTransactionHash] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [showSignModal, setShowSignModal] = useState(false);

  const { setError, myWalletData, setMyWalletData, setTransactions } = useAppContext();
  const { signNewTransaction } = useSignTransaction();
  const { sendTransaction, getTransactions } = useMultiversXApi();

  const formValidation = () => {
    let isValid = true;

    if (!amount || Number(amount) === 0) {
      isValid = false;
      setError(translation.transaction.errors.transactionAmountNeeded)
    } else if (Number(amount) > formatAmount({ input: myWalletData.balance })) {
      isValid = false;
      setError(translation.transaction.errors.transactionAmountExceedBalance)
    } else if (!receiver) {
      isValid = false;
      setError(translation.transaction.errors.receiverNeeded)
    }

    return isValid;
  }

  const onSendTransactionPress = async () => {
    if (!formValidation()) return;

    setShowSignModal(true);
  }

  const onSignTransactionPress = async () => {
    try {
      setShowSignModal(false);
      setIsLoading(true);

      const transaction = await signNewTransaction(receiver, amount, myWalletData.nonce, signPassword);
      const txHash = await sendTransaction(transaction);

      if (txHash) {
        setTransactionHash(txHash);
        const transactions = await getTransactions(Address.fromString(myWalletData.address.value));
        setTransactions(transactions);
      }

      setIsLoading(false);
      setMyWalletData({
        ...myWalletData,
        nonce: myWalletData.nonce + 1,
        balance: myWalletData.balance - amount
      });
    } catch (e) {
      setIsLoading(false);
      setError(e.message);
    }
  }

  return (
    <ContentWrapper>
      {transactionHash ? (
        <Confirmation receiver={receiver} txHash={transactionHash} amount={amount} />
      ) : (
        <>
          <PageTitle title={translation.general.send} />
          <Box flex={1}>
            <BaseInput
              label={`${translation.general.to}:`}
              info={translation.transaction.receiverAddressInfo}
              placeholder={translation.general.addAddress}
              value={receiver}
              onChangeText={value => setReceiver(value)}
            />
            <BaseInput
              label={`${translation.general.amount}:`}
              info={`${translation.transaction.addAmountInfo} ${CURRENCY}`}
              placeholder={translation.general.addAmount}
              value={amount}
              keyboardType="decimal-pad"
              onChangeText={value => setAmount(value)}
            />
          </Box>
          <Button onPress={onSendTransactionPress} isLoading={isLoading} isDisabled={isLoading}>
            {`${translation.general.send} ${translation.general.transaction}`}
          </Button>
        </>
      )}
      <BaseModal
        isOpen={showSignModal}
        onCloseModalPress={() => setShowSignModal(false)}
        title={`${translation.general.sign} ${translation.general.transaction}`}
        confirmButtonTitle={translation.general.sign}
        onConfirmPress={onSignTransactionPress}
      >
        <Box>
          <Text marginBottom={ELEMENT_SPACING.MD}>
            {translation.transaction.signTransactionDetails}
          </Text>
          <BaseInput
            onChangeText={setSignPassword}
            placeHolder={translation.wallet.walletPassword}
            type="password"
          />
        </Box>
      </BaseModal>
    </ContentWrapper>
  );
}

export default TransactionTransfer;
