import React, { useRef, useState } from 'react';
import { WebView } from 'react-native-webview';
import { useAppContext } from '@context/appContext';
import { DAPP_URL } from '@env'
import { Box } from 'native-base';
import useSignTransaction from '@hooks/useSignTransaction';
import useMultiversXApi from '@hooks/useMultiversXApi';
import useToken from '@hooks/useToken';
import ConfirmationModal from './ConfirmationModal';

const translation = require('@config/translation');

const DappTransaction = () => {
  const webview = useRef();
  const token = useToken();
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [transactionData, setTransactionData] = useState(null);
  const { myWalletData, setMyWalletData } = useAppContext();
  const { sendTransaction } = useMultiversXApi();

  const { signGeneratedTransaction } = useSignTransaction();

  const handleOnMessage = ({ nativeEvent: { data }}) => {
    const parsedData = JSON.parse(data);

    if (parsedData?.error) {
      setError(parsedData?.error);
    } else if (parsedData?.type === 'SIGN_TRANSACTIONS_REQUEST') {
      setTransactionData(parsedData?.message?.[0]);
    }
  }

  const onConfirmPress = async () => {
    if (!password) {
      return setError(translation.general.errors.passwordNeeded)
    }

    setIsLoading(true);

    const transaction = await signGeneratedTransaction(transactionData, password);

    await sendTransaction(transaction);

    webview.current.postMessage(`${JSON.stringify({
      fromAdress: myWalletData.address.value,
      message: translation.transaction.transactionSignedWithSuccess
    })}`);

    setMyWalletData({
      ...myWalletData,
      nonce: transactionData.nonce + 1,
      balance: myWalletData.balance - transactionData.value
    });

    setTransactionData(null);
    setIsLoading(false);
  }

  return (
    <Box flex={1}>
      <WebView
        ref={webview}
        source={{ uri: `${DAPP_URL}/?accessToken=${token}` }}
        style={{ flex: 1 }}
        onMessage={handleOnMessage}
        cacheEnabled={false}
        cacheMode="LOAD_NO_CACHE"
      />
      <ConfirmationModal
        show={!!transactionData?.value}
        error={error}
        clearError={() => setError('')}
        receiver={transactionData?.receiver || ''}
        amount={transactionData?.value}
        setPassword={value => setPassword(value)}
        isLoading={isLoading}
        onConfirmPress={onConfirmPress}
      />
    </Box>
  )
};

export default DappTransaction;
