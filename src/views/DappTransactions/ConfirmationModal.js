import React from 'react';
import { CURRENCY } from '@env'
import { Box, Center, Heading, Text } from 'native-base';
import BaseModal from '@components/structure/BaseModal';
import { ELEMENT_SPACING, FONT_SIZE } from '@theme/sizes';
import { formatAmount } from '@multiversx/sdk-dapp/utils/operations';
import BaseInput from '@components/form/BaseInput';
import AppError from '@components/AppError';
import Price from '@components/Price';

const translation = require('@config/translation');

const ConfirmationModal = ({
  show,
  receiver,
  error,
  clearError,
  amount,
  setPassword,
  isLoading,
  onConfirmPress
}) => (
  <BaseModal
    isOpen={show}
    title={`${translation.general.sign} ${translation.general.transaction}`}
    isLoading={isLoading}
    confirmButtonTitle={translation.general.sign}
    onCloseModalPress={() => setTransactionData(null)}
    onConfirmPress={onConfirmPress}
  >
    <Heading marginBottom={ELEMENT_SPACING.MD}>
      {translation.transaction.youNeedToSignTransaction}
    </Heading>
    <AppError message={error} show={!!error} clear={clearError} />
    <Center marginBottom={ELEMENT_SPACING.MD}>
      <Text bold>{translation.general.receiver}:</Text>
      <Text fontSize={FONT_SIZE.MD} textAlign="center">
        {receiver}
      </Text>
    </Center>
    <Center>
      <Text bold>{translation.general.amount}:</Text>
      <Text fontSize={FONT_SIZE.XL}>
        <Price price={amount ? formatAmount({ input: amount }) : 0} />
      </Text>
      <Box width="100%" marginTop={ELEMENT_SPACING.LG}>
        <BaseInput
          placeholder={translation.wallet.walletPassword}
          info={translation.wallet.passwordForSignedTransaction}
          onChangeText={setPassword}
          type="password"
        />
      </Box>
    </Center>
  </BaseModal>
);

export default ConfirmationModal;
