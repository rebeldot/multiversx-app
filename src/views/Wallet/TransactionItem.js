import { Badge, Box, HStack, Text } from 'native-base';
import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { formatAmount } from '@multiversx/sdk-dapp/utils/operations';
import { FONT_SIZE, ELEMENT_SPACING } from '@theme/sizes';
import { CURRENCY } from '@env';
import moment from 'moment';

const TransactionItem = ({ data, myAddress }) => {
  const transactionDirection = myAddress === data.sender ? 'sent' : 'received'; 
  return (
    <HStack marginY={ELEMENT_SPACING.SM} justifyContent="center" alignItems="center" testID="transactionItem">
      <Box width="10%" paddingX={ELEMENT_SPACING.SM}>
        <Icon
          name={transactionDirection === 'sent' ? 'arrow-up' : 'arrow-down'}
          color={transactionDirection === 'sent' ? 'red' : 'green'}
          size={FONT_SIZE.LG}
        />
      </Box>
      <Box width="30%" paddingX={ELEMENT_SPACING.SM}>
        <Text fontSize={FONT_SIZE.SM}>
          {data.txHash}
        </Text>
      </Box>
      <Box width="20%" paddingX={ELEMENT_SPACING.SM}>
        <Text fontSize={FONT_SIZE.SM} color="success.400" bold>
          {formatAmount({ input: data.value})} {CURRENCY}
        </Text>
      </Box>
      <Box width="20%" paddingX={ELEMENT_SPACING.SM}>
        <Badge variant="outline" colorScheme={data.status} _text={{fontSize: FONT_SIZE.SM}}>
          {data.status}
        </Badge>
      </Box>
      <Box width="20%" paddingX={ELEMENT_SPACING.SM}>
        <Text fontSize={FONT_SIZE.SM}>
          {moment(data.timestamp * 1000).format('DD.MM.YYYY')}
        </Text>
      </Box>
    </HStack>
  )
}

export default TransactionItem;
