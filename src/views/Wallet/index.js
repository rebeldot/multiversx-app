import { Box, Button, Center, Divider, ScrollView, Text } from 'native-base';
import { ELEMENT_SPACING, FONT_SIZE } from '@theme/sizes';
import React from 'react';
import { useAppContext } from '@context/appContext';
import { useNavigation } from '@react-navigation/native';
import { formatAmount } from '@multiversx/sdk-dapp/utils/operations';
import TransactionItem from './TransactionItem';
import PageTitle from '@components/structure/PageTitle';
import { CURRENCY } from '@env';

const translation = require("@config/translation.json");

const Wallet = () => {
  const { myWalletData, transactions } = useAppContext();

  const balance = formatAmount({
    input: myWalletData?.balance || 0,
  });

  const myAddress = myWalletData.address?.value;

  const navigation = useNavigation();

  const onSendTransactionPress = () => {
    navigation.navigate('TransactionTransfer');
  }

  return (
    <ScrollView  backgroundColor="black" testID='walletWrapper'>
      <Box flex={1}>
        <Center padding={ELEMENT_SPACING.MD}>
          <PageTitle title={translation.general.myWallet} />
          <Center marginY={ELEMENT_SPACING.MD}>
            <Text color="grey">{translation.general.address}:</Text>
            <Text textAlign="center" testID="address">{myAddress}</Text>
          </Center>
          <Center marginY={ELEMENT_SPACING.MD}>
            <Text color="grey">{translation.general.balance}:</Text>
            <Text color="success.400" testID="balance">
              <Text bold>
                {balance || 0}
              </Text> {CURRENCY}
            </Text>
          </Center>
          <Button onPress={onSendTransactionPress} testID="sendTransactionButton">
            {`${translation.general.send} ${translation.general.transaction}`}
          </Button>
          <Divider marginY={ELEMENT_SPACING.XL} />
        </Center>
        <Center>
          {transactions.length === 0 ? (
            <Text fontSize={FONT_SIZE.LG} bold color="danger.400">
              {translation.transaction.errors.noTransactions}
            </Text>
          ) : (
            <>
              <Text fontSize={FONT_SIZE.LG} testID="transactionsNoHeading">
                Last <Text bold>{transactions.length}</Text> transactions for account:
              </Text>
              <Box width="100%" marginY={ELEMENT_SPACING.LG}>
                {transactions.map(transaction => (
                  <TransactionItem key={transaction.txHash} data={transaction} myAddress={myAddress} />
                ))}
              </Box>
            </>
          )}
        </Center>
      </Box>
    </ScrollView>
  );
}

export default Wallet;