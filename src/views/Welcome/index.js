import React, { useState, useEffect, useTransition } from 'react';
import { Box, Button, Center, Image, Text, TextArea, Radio, Link, ScrollView } from 'native-base';
import { ELEMENT_SPACING } from '@theme/sizes';
import { mnemonicValidation, getExistingWalletFiles, saveWalletFile, getWalletFromExistingFile } from '@utils/wallet';
import useMultiversXApi from '@hooks/useMultiversXApi';
import { UserWallet, UserSigner } from "@multiversx/sdk-wallet";
import { useAppContext } from '@context/appContext';
import PageTitle from '@components/structure/PageTitle';
import BaseInput from '@components/form/BaseInput';

const LOGO = require('@images/logo.png')

const translation = require("@config/translation.json");

const Welcome = () => {
  const [words, setWords] = useState('');
  const [password, setPassword] = useState('');
  const [files, setFiles] = useState([]);
  const [existingWalletFile, setExistingWalletFile] = useState('');
  const [showExistingFiles, setShowExistingFiles] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const { getAccount, getTransactions, getNetworkConfig } = useMultiversXApi();
  const { setMyWalletData, setTransactions, setNetworkConfig, setError } = useAppContext();

  useEffect(() => {
    (async () => {
      const files = await getExistingWalletFiles()

      if (files.length > 0) {
        setExistingWalletFile(files[0].name)
        setFiles(files);
        setShowExistingFiles(true);
      }
    })();
  }, []);

  const onAddAnotherWalletPress = () => setShowExistingFiles(false);

  const onChooseFromExistingFilesPress = () => setShowExistingFiles(true);

  const existingFileFlow = async () => {
    const walletFile = await getWalletFromExistingFile(existingWalletFile.replace('.json', ''), password);
    
    const address = walletFile?.getAddress();

    const myWallet = address ? await getAccount(address) : null;

    return myWallet;
  };

  const newFileFlow = async () => {
    const { error, mnemonic } = mnemonicValidation(words);

    if (error) {
      return setError(error);
    }
    
    const wallet = UserWallet.fromMnemonic({
      mnemonic,
      password
    });

    const account = UserSigner.fromWallet(wallet.toJSONWhenKindIsMnemonic(), password);

    const address = account.getAddress();
    const myWallet = await getAccount(address);

    await saveWalletFile(address.bech32(), wallet.toJSON());

    return myWallet;
  }

  const onImportWalletPress = async () => {
    let myWallet;

    setError('');
    setIsLoading(true);

    if (!password) {
      setIsLoading(false);
      return setError(translation.wallet.errors.password)
    }

    requestAnimationFrame(async () => {
      try {
        if (existingWalletFile && showExistingFiles) {
          myWallet = await existingFileFlow();
        } else {
          myWallet = await newFileFlow();
        }
    
        const transactions = await getTransactions(myWallet?.address.value);
        setTransactions(transactions);
  
        const config = await getNetworkConfig();
        setNetworkConfig(config);
  
        setMyWalletData(myWallet);
  
        setIsLoading(false);
      } catch (e) {
        setError(e.message);
        setIsLoading(false);
      }
    });
  }

  return (
    <ScrollView flex={1} paddingX={ELEMENT_SPACING.MD} backgroundColor="black">
      <Center marginY={ELEMENT_SPACING.LG}>
        <Image
          source={LOGO}
          alt="MultiversX logo"
          resizeMode="contain"
          height={50}
          marginBottom={ELEMENT_SPACING.XL}
        />
        <PageTitle title={translation.welcome.welcomeToMultivers} />
        <Text italic>{translation.welcome.beginYourExperience}</Text>
      </Center>
      <Box flex={1}>
        {showExistingFiles ? (
          <Box marginY={ELEMENT_SPACING.XL}>
            <Text>{translation.wallet.existingWalletFiles}:</Text>
            <Box marginBottom={ELEMENT_SPACING.MD}
              paddingX={ELEMENT_SPACING.SM}
              paddingY={ELEMENT_SPACING.MD}
              borderWidth={.5}
              borderColor="gray.400"
              borderRadius={5}
            >
              <Radio.Group name="walletFiles" defaultValue={existingWalletFile} onChange={value => setExistingWalletFile(value)}>
                {files.map(file => (
                  <Box key={file.name} marginY={ELEMENT_SPACING.SM}>
                    <Radio value={file.name}>
                      {/* 90% because nativebase radio pushes text outside screen (quick-fix) */}
                      <Text width="90%">{file.name}</Text>
                    </Radio>
                  </Box>
                ))}
              </Radio.Group>
            </Box>
            <Link onPress={onAddAnotherWalletPress}>
              {translation.wallet.addWallet}
            </Link>
          </Box>
        ) : (
          <Box marginY={ELEMENT_SPACING.XL}>
            <TextArea
              placeholder={`${translation.general.writeYourWords}...`}
              variant="outline"
              value={words}
              onChangeText={text => setWords(text)}
              numberOfLines={10}
              h={150}
              marginBottom={ELEMENT_SPACING.MD}
            />
            {files.length > 0 && (
              <Link onPress={onChooseFromExistingFilesPress}>
                {translation.welcome.chooseFromExistingFiles}
              </Link>
            )}
          </Box>

        )}
        <BaseInput
          value={password}
          placeholder={translation.wallet.walletPassword}
          info={translation.wallet.walletPasswordInfo}
          type="password"
          onChangeText={value => setPassword(value)}
        />
      </Box>
      <Button
        marginY={ELEMENT_SPACING.LG}
        onPress={onImportWalletPress}
        disabled={isLoading}
        isLoading={isLoading}
      >
        {translation.wallet.importMyWallet}
      </Button>
    </ScrollView>
  );
}

export default Welcome;