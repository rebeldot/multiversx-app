import { Transaction, TokenTransfer } from "@multiversx/sdk-core";
import { useAppContext } from '@context/appContext';
import { getWalletFromExistingFile } from '@utils/wallet';

const useSignTransaction = () => {
  const { myWalletData, networkConfig } = useAppContext();
  
  if (!networkConfig?.MinTransactionVersion) {
    throw new Error('Could not get transaction config.')
  }

  return {
    signNewTransaction: async (receiver, amount, nonce, password) => {
      try {
        const transactionObj = {
          gasLimit: networkConfig.MinGasLimit,
          gasPrice: networkConfig.MinGasPrice,
          receiver,
          chainID: networkConfig.ChainID,
          version: networkConfig.MinTransactionVersion,
          nonce,
        };
  
        const wallet = await getWalletFromExistingFile(myWalletData.address.value, password);
  
        if (wallet) {
          transactionObj.sender = myWalletData.address.value;
          transactionObj.value = TokenTransfer.egldFromAmount(amount);
  
          const transaction = Transaction.fromPlainObject(transactionObj);
          
          const serializedTransaction = transaction.serializeForSigning();
  
          const transactionSignature = await wallet.sign(serializedTransaction);
      
          transaction.applySignature(transactionSignature);
  
          return transaction;
        }
      
        throw new Error('Could not locate signin file.');
      } catch (e) {
        throw new Error(e.message);
      }
    },

    signGeneratedTransaction: async (transactionObj, password) => {
      try {
        const wallet = await getWalletFromExistingFile(myWalletData.address.value, password);

        if (wallet) {
          const transaction = Transaction.fromPlainObject(transactionObj);

          const serializedTransaction = transaction.serializeForSigning();
  
          const transactionSignature = await wallet.sign(serializedTransaction);
      
          transaction.applySignature(transactionSignature);
  
          return transaction;
        }
      
        throw new Error('Could not locate signin file.');
      } catch (e) {
        throw new Error(e.message);
      }
    }
  }
}

export default useSignTransaction;
