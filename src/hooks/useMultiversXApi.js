import { ApiNetworkProvider } from "@multiversx/sdk-network-providers";
import { BASE_API_URL } from '@env';

const useMultiversXApi = () => {
  const networkProvider = new ApiNetworkProvider(BASE_API_URL);

  return {
    getAccount: async (address) => {
      try {
        const response = await networkProvider.getAccount(address);

        return response;
      } catch (e) {
        throw new Error(e.message);
      }
    },

    getTransactions: async (address) => {
      try {
        const response = await networkProvider.doGetGeneric(`accounts/${address}/transactions`);

        return response;
      } catch (e) {
        throw new Error(e.message);
      }
    },

    sendTransaction: async (payload) => {
      try {
        const response = await networkProvider.sendTransaction(payload);

        return response;
      } catch (e) {
        throw new Error(e.message);
      }
    },

    getNetworkConfig: async () => {
      try {
        const response = await networkProvider.getNetworkConfig();

        return response;
      } catch (e) {
        throw new Error(e.message);
      }
    },
  };
}

export default useMultiversXApi;