import { useState } from 'react';
import { nativeAuth } from '@multiversx/sdk-dapp/services/nativeAuth/nativeAuth';
import { SignableMessage } from "@multiversx/sdk-core/out";
import { useAppContext } from '@context/appContext';
import { BASE_API_URL, DAPP_URL } from '@env'

const useToken = () => {
  const [token, setToken] = useState('');
  const { myWalletData } = useAppContext();

  const nativeAuthManager = nativeAuth();

  if (myWalletData?.address?.value) {
    (async () => {

      const nativeAuthConfig = nativeAuthManager.getNativeAuthConfig(true);

      nativeAuthConfig.origin = DAPP_URL,
      nativeAuthConfig.apiAddress = BASE_API_URL;

      const tempToken = await nativeAuthManager.initialize();
      const signedMessage = new SignableMessage({
        message: Buffer.from(myWalletData.address?.value + tempToken)
      });

      const accessToken = nativeAuthManager.getToken({
        address: myWalletData.address.value,
        token: tempToken,
        signature: signedMessage.serializeForSigning()
      });
  
      setToken(accessToken);
    })();
  }

  return token;
}

export default useToken;