import { Box, Text } from 'native-base';
import React from 'react';
import { CURRENCY } from '@env';

const Price = ({ price }) => (
  <Box>
    <Text bold color="success.400">{price} {CURRENCY}</Text>
  </Box>
);

export default Price;
