import React from 'react';
import { Box, Text, TextField } from 'native-base';
import { ELEMENT_SPACING, FONT_SIZE } from '@theme/sizes';

const BaseInput = ({
  label,
  type = 'text',
  value,
  info,
  onChangeText,
  placeholder,
  keyboardType
}) => {
  return (
    <Box marginBottom={ELEMENT_SPACING.LG}>
      {label && <Text marginBottom={ELEMENT_SPACING.MD}>{label}</Text>}
      <TextField
        type={type}
        value={value}
        placeholder={placeholder}
        onChangeText={onChangeText}
        keyboardType={keyboardType}
      />
      {info && <Text fontSize={FONT_SIZE.SM} color="grey">{info}</Text>}
    </Box>
  )
}

export default BaseInput;
