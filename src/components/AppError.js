import { Alert, HStack, Box, Text } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { ELEMENT_SPACING, FONT_SIZE } from '@theme/sizes';
import React from 'react';

const AppError = ({ show, message, clear }) => {
  if (!show) return null;

  return (
    <Alert
      status="error"
      top={2}
      alignSelf="center"
      width="100%"
      position="absolute"
      zIndex={1}
      opacity={.96}
    >
      <HStack alingItems="center">
        <Box flexDirection="row" justifyContent="flex-start" width="90%">
          <Icon name="exclamation-circle" size={FONT_SIZE.XL} color="red" style={{ alignSelf: 'center' }} />
          <Text marginLeft={ELEMENT_SPACING.SM} fontSize={FONT_SIZE.MD} color="red.600">
            {message}
          </Text>
        </Box>
        <Box>
          <Icon name="times" size={FONT_SIZE.XL} onPress={() => clear('')}  style={{ alignSelf: 'flex-end' }} />
        </Box>
      </HStack>
    </Alert>
  )
}

export default AppError;
