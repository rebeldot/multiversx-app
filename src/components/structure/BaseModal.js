import React from 'react';
import { Button, Modal } from 'native-base';

const BaseModal = ({
  isOpen = false,
  onCloseModalPress,
  title,
  children,
  isLoading,
  confirmButtonTitle = "Confirm",
  onConfirmPress,
  cancelButtonTitle = "Cancel",
}) => {
  return (
    <Modal isOpen={isOpen} onClose={onCloseModalPress} size="full">
      <Modal.Content>
          <Modal.CloseButton />
          <Modal.Header>{title}</Modal.Header>
          <Modal.Body>
            {children}
          </Modal.Body>
          <Modal.Footer>
            <Button.Group space={2}>
              <Button variant="ghost" backgroundColor="gray.200" onPress={onCloseModalPress}>
                {cancelButtonTitle}
              </Button>
              <Button onPress={onConfirmPress} isDisabled={isLoading} isLoading={isLoading}>
                {confirmButtonTitle}
              </Button>
            </Button.Group>
          </Modal.Footer>
        </Modal.Content>
    </Modal>
  )
}

export default BaseModal;
