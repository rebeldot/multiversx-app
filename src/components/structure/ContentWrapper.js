import React from 'react';
import { Box } from 'native-base';
import { ELEMENT_SPACING } from '@theme/sizes';

const ContentWrapper = ({ children }) => {
  return (
    <Box flex={1} padding={ELEMENT_SPACING.MD} backgroundColor="black">
      {children}
    </Box>
  )
}

export default ContentWrapper;
