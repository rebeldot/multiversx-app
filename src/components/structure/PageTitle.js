import React from 'react';
import { Center, Heading } from 'native-base';
import { ELEMENT_SPACING } from '@theme/sizes';

const PageTitle = ({ title }) => (
  <Center marginY={ELEMENT_SPACING.LG}>
    <Heading color="white">
      {title}
    </Heading>
  </Center>
)

export default PageTitle;
