import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import WalletNavigation from './walletNavigation';
import DappTransactions from '@views/DappTransactions';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { FONT_SIZE } from '@theme/sizes';

const Tab = createBottomTabNavigator();

function BottomTabs() {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarActiveTintColor: '#23f7dd',
        tabBarStyle: { backgroundColor: 'black' },
        unmountOnBlur: true
      }}
    >
      <Tab.Screen
        name="WalletTabs"
        component={WalletNavigation}
        options={{ tabBarIcon: ({ color }) => <Icon name="wallet" size={FONT_SIZE.LG} color={color} /> }}
      />
      <Tab.Screen
        name="dApp"
        component={DappTransactions}
        options={{
          tabBarIcon: ({ color }) => <Icon name="money-bill" size={FONT_SIZE.LG} color={color} /> 
        }}
      />
    </Tab.Navigator>
  );
}

export default BottomTabs;
