import { useAppContext } from '@context/appContext';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Welcome from '@views/Welcome';
import BottomTabs from './bottomTabs';

const Stack = createNativeStackNavigator();

const AppNavigator = () => {
  const { myWalletData } = useAppContext();

  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      {!myWalletData?.address?.value ? (
        <Stack.Screen name="Welcome" component={Welcome} />
      ) : (
        <Stack.Screen name="BottomTabs" component={BottomTabs} />
      )}
    </Stack.Navigator>
  );
}

export default AppNavigator
