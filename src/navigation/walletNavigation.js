import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Wallet from '@views/Wallet';
import TransactionTransfer from '@views/TransactionTransfer';

const Stack = createNativeStackNavigator();

const WalletNavigation = () => (
  <Stack.Navigator screenOptions={{ headerShown: false }}>
    <Stack.Screen name="Wallet" component={Wallet} />
    <Stack.Screen name="TransactionTransfer" component={TransactionTransfer} />
  </Stack.Navigator>
);

export default WalletNavigation