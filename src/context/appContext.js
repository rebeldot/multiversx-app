import React, { createContext, useContext, useState } from 'react';
import AppError from '@components/AppError';

export const AppContext = createContext();

export const useAppContext = () => useContext(AppContext);

const AppProvider = ({ children }) => {
  const [myWalletData, setMyWalletData] = useState({});
  const [transactions, setTransactions] = useState([]);
  const [error, setError] = useState('');
  const [networkConfig, setNetworkConfig] = useState({});
  const [nonce, setNonce] = useState(0);

  return (
    <AppContext.Provider
      value={{
        myWalletData,
        setMyWalletData,
        transactions,
        setTransactions,
        setError,
        networkConfig,
        setNetworkConfig,
        nonce,
        setNonce
      }}
    >
      <>
        {children}
        <AppError message={error} show={!!error} clear={setError} />
      </>
    </AppContext.Provider>
  )
}

export default AppProvider;
