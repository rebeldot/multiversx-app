import { extendTheme } from 'native-base';

export const theme = extendTheme({
  colors: {
    info: {
      400: '#23f7dd',
    },
  },
  components: {
    Button: {
      defaultProps: {
        backgroundColor: '#23f7dd',
        _text: {
          color: 'black'
        },
      },
    },
  },
  config: {
    // Changing initialColorMode to 'dark'
    initialColorMode: 'dark',
  },
});