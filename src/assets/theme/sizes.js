const ELEMENT_SPACING = {
  SM: 2,
  MD: 4,
  LG: 6,
  XL: 8
}

const FONT_SIZE = {
  SM: 10,
  MD: 14,
  LG: 18,
  XL: 22,
  XXL: 26,
  XXXL: 30
}

export {
  ELEMENT_SPACING,
  FONT_SIZE
}

