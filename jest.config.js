module.exports = {
  preset: 'react-native',
  moduleFileExtensions: ['js', 'jsx', 'json', 'node'],
  setupFilesAfterEnv: ['@testing-library/jest-native/extend-expect'],
  "transformIgnorePatterns": [
    "node_modules/(?!(@react-native|react-native|react-native-vector-icons)/)"
  ],
  moduleNameMapper: {
    '@multiversx/sdk-dapp/(.*)':
      '<rootDir>/node_modules/@multiversx/sdk-dapp/__commonjs/$1'
  }
};
